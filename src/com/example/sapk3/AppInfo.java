package com.example.sapk3;

import org.json.JSONObject;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class AppInfo {
	public String appName = "";
	public String packageName = "";
	public String versionName = "";
	public int versionCode = 0;
	public Drawable appIcon = null;
	public String cateName = null;
	public int isSys = 0;
		
	public AppInfo(PackageManager pm, PackageInfo packageInfo) {
		this.appName = packageInfo.applicationInfo.loadLabel(pm).toString();
		this.packageName = packageInfo.packageName;
		this.versionName = packageInfo.versionName;
		this.versionCode = packageInfo.versionCode;
		this.appIcon = packageInfo.applicationInfo.loadIcon(pm);
		if ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) > 0 ){
	        this.isSys = 1;   
	    }
	}

	public void print() {
		Log.i("app", "Name:" + appName + " Package:" + packageName);
		Log.i("app", "Name:" + appName + " versionName:" + versionName);
		Log.i("app", "Name:" + appName + " versionCode:" + versionCode);
	}

	public JSONObject getJSONObject() {
		JSONObject jo = new JSONObject();
		try {
			jo.put("appName", this.appName);
			jo.put("packageName", this.packageName);
			jo.put("versionCode", this.versionCode);
			jo.put("versionName", this.versionName);
			jo.put("isSys", this.isSys);
		} catch (Exception e) {

		} finally {

		}
		return jo;
	}
}
