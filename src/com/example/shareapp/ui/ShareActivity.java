package com.example.shareapp.ui;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

import com.example.shareapp.R;
import com.example.shareapp.control.AppDataHelper;
import com.example.shareapp.control.HandlerControl;
import com.example.shareapp.data.AppInfoItem;


public class ShareActivity extends Activity{
	private ArrayList<AppInfoItem> appearAppInfoItemList = new ArrayList<AppInfoItem>();
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		HandlerControl.getInstance();
		new Thread(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				super.run();
				AppDataHelper.getInstance().getInstalledPackageTitles(ShareActivity.this);
			}
			
		}.start();
		setContentView(R.layout.activity_main);
		
		
		
//		
//		// tjx ExistsCheck
//		ArrayList<String> pnames = new ArrayList<String>();
//		pnames.add("org.wikipedia");
//		pnames.add("xxx.wikipedia");
//		
//		// 1. 查询是否存在
//		ExistsChecker ec = new ExistsChecker(pnames);
//		HashMap<String, Number> map = ec.check();
//		Log.v("tjx", map.toString());
//		
//		// 2. 获取短分享链接
//		String pname = "org.wikipedia";
//		Log.v("tjx", pname + ": " + "http://sapk.tjx.be/s.php?m=" + ExistsChecker.getMda(pname));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		HandlerControl.getHandlerControl().arrayList.clear();
		super.onDestroy();
	}
	
	
}
