package com.example.shareapp.receiver;

import com.example.shareapp.control.AppDataHelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class WifiReceiver extends BroadcastReceiver {

	 @Override
	    public void onReceive(Context context, Intent intent) {     
	        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE); 
	        NetworkInfo netInfo = conMan.getActiveNetworkInfo();
	        if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
	        	Log.i("tag", "wifi has been connected");
	        	AppDataHelper.getInstance().initCloudStatus();
	        }
	        else{
	        	Log.i("tag", "wifi has been disconnected");
	        }
	    }   

}
